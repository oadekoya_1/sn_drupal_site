<?php
/**
 * @file
 * custom_article.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function custom_article_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_url|node|article|default';
  $field_group->group_name = 'group_text_url';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text URL group',
    'weight' => '2',
    'children' => array(
      0 => 'field_long_text',
      1 => 'field_url',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-text-url field-group-fieldset',
      ),
    ),
  );
  $export['group_text_url|node|article|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_text_url|node|article|form';
  $field_group->group_name = 'group_text_url';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'article';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Text URL group',
    'weight' => '2',
    'children' => array(
      0 => 'field_long_text',
      1 => 'field_url',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Text URL group',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-text-url field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $export['group_text_url|node|article|form'] = $field_group;

  return $export;
}
